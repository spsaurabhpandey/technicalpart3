# technicalPart3

This project has automated tests on weatherstack api<br>

# How to run postman/newman tests in your local desktop?
<br>How to run the cypress tests in you local desktop?
<br>Step1: git clone https://gitlab.com/spsaurabhpandey/technicalpart3.git
<br>Step2: open the contents of this repository in IDE.
<br>Step3: Run following commands in terminal
<br>a) npm install
<br>b) npm run test

# Where can I find the html report?
report.html in technicalPart3 folder i.e. root folder of the project.

# Is there a CI pipeline configured for this project?
Yes, Checkout the CI Pipeline in CICD section of this repository and don't forget to donwload the html report as artifact of the pipeline.
